# Welcome to Abrawser

**Abra + Browser = Abrawser. A site-specific browser for Free / Open Source cloud apps.**

Abrawser requires the latest version of Xulrunner to run. You can download
Xulrunner from:

    http://ftp.mozilla.org/pub/mozilla.org/xulrunner/releases/

On Linux, download the `.tar.bz2` and extract it to an accessible directory.
Eg. `/usr/local/xulrunner`

To run this browser, just download the code and then execute:

    /usr/local/xulrunner/xulrunner application.ini

(On Linux currently - other platforms coming soon.)
